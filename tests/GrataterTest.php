<?php
use PHPUnit\Framework\TestCase;
use Munspel\Helper\Gravatar;
class GravatarTest extends TestCase
{
    public function testHashMethod(){
        $this->assertTrue(Gravatar::hash("abcd@mail.ru") == Gravatar::hash("ABcd@mail.ru"));
        $this->assertTrue(Gravatar::hash("abcd@mail.ru") == Gravatar::hash(" ABcd@mail.ru "));
        $this->assertTrue(Gravatar::hash(" abcd@mail.ru") == Gravatar::hash(" ABcd@Mail.ru"));
        $this->assertFalse(Gravatar::hash("abcd@mail.ru") == Gravatar::hash("cba@mail.ru"));
    }
}