<?php
/**
 * Created by PhpStorm.
 * User: Munspel
 * Date: 08.10.2019
 * Time: 16:37
 */

namespace Munspel\Helper;


final class Gravatar
{

    /**
     * Get either a Gravatar URL or complete image tag for a specified email address.
     *
     * @param string $email The email address
     * @param string $s Size in pixels, defaults to 80px [ 1 - 2048 ]
     * @param string $d Default imageset to use [ 404 | mp | identicon | monsterid | wavatar ]
     * @param string $r Maximum rating (inclusive) [ g | pg | r | x ]
     * @param boole $img True to return a complete IMG tag False for just the URL
     * @param array $atts Optional, additional key/value attributes to include in the IMG tag
     * @return String containing either just a URL or a complete image tag
     * @source https://gravatar.com/site/implement/images/php/
     */
    public static function get($email, $s = 80, $d = 'mp', $r = 'g', $img = false, $atts = array())
    {
        $url = 'https://www.gravatar.com/avatar/';
        $url .= self::hash($email);
        $url .= "?s=$s&d=$d&r=$r";
        if ($img) {
            $url = '<img src="' . $url . '"';
            foreach ($atts as $key => $val)
                $url .= ' ' . $key . '="' . $val . '"';
            $url .= ' />';
        }
        return $url;
    }

    /**
     * @param $email
     * @return string
     */
    public static function hash($email)
    {
        return md5(strtolower(trim($email)));
    }

    /**
     * @param $email
     * @return mixed|null
     */
    public static function profile($email)
    {
        $hash = self::hash($email);
        $str = file_get_contents("https://www.gravatar.com/$hash.php");
        $profile = unserialize($str);
        if (is_array($profile) && isset($profile['entry'])) {
            return $profile;
        }
        return null;
    }

}